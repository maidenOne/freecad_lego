#-------------------------
# Author: Anton Fosselius 2019
#-------------------------
#  Description: opens all .FCStd files in [filedir] and renders images in [filetype] format to [outdir] in [imageWidth]x[imageHeight] size
#-------------------------
# HowTo:
# copy paste the following line to your freecad python console:
# execfile("[full path to]thumbnails.py")
# example: execfile("/home/maiden/Projects/freecad_lego/Scripts/thumbnails.py")

import os,FreeCAD,Part,PartGui

path = "/home/maiden/Projects"
filedir =  path + "/freecad_lego/Parts/"
outdir = path + "/freecad_lego/img/"
filetype = ".jpg"
imageWidth = 128
imageHeight = 128

def shoot(filename):
	print filedir + filename
	FreeCAD.open(filedir + filename)
	Gui.activeDocument().activeView().viewAxonometric()
	Gui.SendMsgToActiveView("ViewFit")
	Gui.ActiveDocument.ActiveView.saveImage(outdir + filename.strip('.FCStd') + filetype,imageWidth,imageHeight,"White")

	#NOTE close all documents! (too lazy to figure out what the key of the recent opened document is.. have tested [0] and [-1], but order is random? )
	for key in App.listDocuments().keys():
		App.closeDocument(key)

for filename in os.listdir(filedir):
	if filename.endswith(".FCStd"):
		print filename.strip(".FCStd")
		shoot(filename)

