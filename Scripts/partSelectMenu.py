#-------------------------
# Author: Anton Fosselius 2019
#-------------------------
#  Description: opens previews of images if available
#-------------------------

from PySide import QtCore, QtGui
from PyQt4.QtGui import *
import os

def gotEvent(event):
    print "event: " + str(event)

app = QtGui.QApplication([])
win = QWidget()
#win.setFixedSize(100,100)
layout = QHBoxLayout()
# Read an image from file and creates an ARGB32 string representing the image
def addButton(name):
    button = QPushButton(str(name))
    #button.setIcon(QIcon(QPixmap("test.png")))
    button.clicked.connect(lambda:gotEvent(str(name)))
    button.setStyleSheet("color:'red'; font-size:20px; font:bold; width: 128px; height: 128px; background-image: url('/home/maiden/Projects/freecad_lego/img/"+name+".jpg'); border: none;")
    return button;

for filename in sorted(os.listdir("/home/maiden/Projects/freecad_lego/Parts/")):
	if filename.endswith(".FCStd"):
		print filename.strip(".FCStd")
		layout.addWidget(addButton(filename.strip(".FCStd")))

win.setLayout(layout)

scrollArea = QScrollArea()
scrollArea.setWidget(win)
scrollArea.show()
app.exec_()
